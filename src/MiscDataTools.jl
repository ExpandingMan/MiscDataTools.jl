module MiscDataTools

using LinearAlgebra
using Transducers
using StatsBase
using Tables


struct Inverse{F}
    𝒻::F
end

(ℊ::Inverse)(a...; kw...) = inv(ℊ.𝒻, a...; kw...)


include("ordinalmap.jl")
include("show.jl")


export OrdinalMap, MultiOrdinalMap

end
