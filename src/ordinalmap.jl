
"""
    OrdinalMap{T}

An object representing the map

``\\phi : S \\mapsto \\mathbb{Z}^{1}_n``

where ``S`` is an arbitrary set and ``\\mathbb{Z}^{1}_n`` is ``\\mathbb{Z}_n`` with
``1`` added to each element.  ``\\phi`` is invertible and the codomain is specifically
chosen so that it can be used as the index of a rank-1 array.

## Missing Values
The above is only exactly true in the absence of missing values.  `OrdinalMap` allows
for the existence of a special "missing" value, meaning that any object not explicitly
given as part of the set ``S`` will be mapped to the integer ``1``.
Conversely, rather than providing a true inverse, ``1`` will be mapped to a special
"missing" value, `missing` by default.

## Use Cases
Very frequently, when dealing with badly formatted data, one is left holding a set of completely
useless objects with little or no semantic meaning which are better off being represented as integers.
This is a similar approach to `CategoricalArrays`, but whereas `CategoricalArray` allows users to
deal with the original values backed by a sane index, `OrdinalMap` allows users to completely
discard the original values in favor of sane indices.
"""
struct OrdinalMap{T,M}
    domain::Vector{Union{M,T}}
    map::Dict{Union{M,T},Int}
    name::Symbol  # should be thought of as name of set S
end

Base.inv(ϕ::OrdinalMap) = Inverse(ϕ)

Base.length(ϕ::OrdinalMap) = length(ϕ.domain)

domain(ϕ::OrdinalMap) = ϕ.domain

function OrdinalMap(S;
                    name::Symbol=:x1,
                    use_missing::Bool=true,
                    missing_val=missing,
                    sort::Bool=true,
                   )
    ot = eltype(S)
    mt = use_missing ? typeof(missing_val) : Union{}
    S = collect(Union{mt,eltype(S)}, S)
    sort && sort!(S)
    use_missing && pushfirst!(S, missing_val)
    mp = Dict{eltype(S),Int}(S .=> 1:length(S))
    OrdinalMap{ot,mt}(S, mp, name)
end
OrdinalMap(ϕ::OrdinalMap) = ϕ

usesmissing(ϕ::OrdinalMap{T,M}) where {T,M} = (M ≠ Union{})

function missingval(ϕ::OrdinalMap)
    usesmissing(ϕ) || error("OrdinalMap $(ϕ.name) does not support missing values")
    first(ϕ.domain)
end

(ϕ::OrdinalMap{T,Union{}})(s) where {T} = ϕ.map[s]

(ϕ::OrdinalMap{T,M})(s) where {T,M} = get(ϕ.map, s, 1)

Base.inv(ϕ::OrdinalMap, k::Integer) = ϕ.domain[k]

forward(::Type{Dict}, ϕ::OrdinalMap) = ϕ.map


"""
    MultiOrdinalMap

An object representing the map

``\\Phi : S_1 \\otimes \\cdots \\otimes S_N \\mapsto \\mathbb{Z}^{1}_{n_1}\\otimes\\cdots\\otimes\\mathbb{Z}^{1}_{n_N}``

where the ``S_j`` are arbitrary sets and ``\\mathbb{Z}^{1}_n`` is ``\\mathbb{Z}_n`` with ``1`` added to each element.
``\\Phi`` is invertible and the codomain is specifically chosen so that it can be used to index a rank ``N`` array.

This is essentially an outer product of [`OrdinalMap`](@ref) objects
```julia
Φ = ϕ₁ ⊗ ϕ₂
```
creates a `MultiOrdinalMap` `Φ` from `OrdinalMap`s `ϕ₁` and `ϕ₂`.
"""
struct MultiOrdinalMap{T<:Tuple}
    names::Vector{Symbol}
    names_lookup::Dict{Symbol,Int}
    maps::T
end

Base.inv(Φ::MultiOrdinalMap) = Inverse(Φ)

Base.size(Φ::MultiOrdinalMap) = ntuple(j -> length(Φ.maps[j]), length(Φ.maps))

Base.names(Φ::MultiOrdinalMap) = Φ.names

getmap(Φ::MultiOrdinalMap, j::Integer) = Φ.maps[j]
getmap(Φ::MultiOrdinalMap, name::Symbol) = getmap(Φ, Φ.names_lookup[name])

getmaps(Φ::MultiOrdinalMap) = Φ.maps

function domain(Φ::MultiOrdinalMap)
    dms = (domain(ϕ) for ϕ ∈ getmaps(Φ))
    (collect(p) for p ∈ Iterators.product(dms...))
end

function MultiOrdinalMap(names, maps)
    names = collect(names)
    maps = Tuple(maps)
    lookup = Dict(names .=> 1:length(names))
    MultiOrdinalMap{typeof(maps)}(names, lookup, maps)
end

#TODO: outer products with `MultiOrdinalMap` arguments
function (⊗)(ϕs::OrdinalMap...)
    names = ϕs |> Map(ϕ -> ϕ.name) |> collect
    MultiOrdinalMap(names, ϕs)
end

function _MultiOrdinalMap_maketable(tbl)
    tbl = Tables.columntable(tbl)
    keys(tbl) |> Map(k -> k=>unique(tbl[k])) |> NamedTuple
end

function MultiOrdinalMap(tbl; kw...)
    Tables.istable(tbl) && (tbl = _MultiOrdinalMap_maketable(tbl))
    MultiOrdinalMap(keys(tbl), (OrdinalMap(v; name=k, kw...) for (k,v) ∈ pairs(tbl)))
end

(Φ::MultiOrdinalMap)(Ss) = Ss |> Enumerate() |> MapSplat((j, s) -> getmap(Φ, j)(s)) |> Tuple

flattened(Φ::MultiOrdinalMap, Ss) = flatten(Φ, Φ(Ss))

function (Φ::MultiOrdinalMap)(nt::NamedTuple)
    zip(names(Φ), Φ.maps) |> Map() do (n, ϕ)
        if n ∈ keys(nt)
            ϕ(nt[n])
        else
            usesmissing(ϕ) || throw(ArgumentError("invalid argument. $(ϕ.name) must have value"))
            ϕ(get(nt, n, first(domain(ϕ))))
        end
    end |> Tuple
end

Base.inv(Φ::MultiOrdinalMap, v) = zip(getmaps(Φ), v) |> MapSplat((ϕ, x) -> inv(ϕ, x)) |> Tuple

Base.inv(Φ::MultiOrdinalMap, n::Integer) = inv(Φ, unflatten(Φ, n))

_flatten_coeff(ls, j::Integer) = j == 1 ? 1 : prod(ls[1:(j-1)])

# this looks ok so far...
"""
    flatten(Φ::MultiOrdinalMap, K)

Given a set of indices from the `MultiOrdinalMap`, flatten them to a single integer index.
This is analagous to flattening a cartesian index to a sequential 1-dimensional index,
though this version is more general as the co-domains of each constituent map needn't all
have the same size.
"""
function flatten(Φ::MultiOrdinalMap, K)
    o = 0
    ls = size(Φ)
    for (j, k) ∈ enumerate(K)
        k > ls[j] && error("invalid index")
        o += (Int(k)-1)*_flatten_coeff(ls, j)
    end
    o+1
end

#TODO: this allocates in order to iterate backwards, not ideal
"""
    unflatten(Φ::MultiOrdinalMap, n::Integer)

Given a 1-dimensional index for the `MultiOrdinalMap`, expand it into a mulit-dimensional
index.  This is analagous to unflattening a 1-dimensional index to a cartesian index,
though this version is more general as the co-domains of each constituent map needn't all
have the same size.
"""
function unflatten(Φ::MultiOrdinalMap, n::Integer)
    ls = size(Φ)
    o = Vector{Int}(undef, length(ls))
    N = n
    for j ∈ length(ls):(-1):2
        (a, N) = fldmod1(N, _flatten_coeff(ls, j))
        o[j] = a
    end
    o[1] = N
    Tuple(o)
end
