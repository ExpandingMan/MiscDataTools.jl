
function Base.show(io::IO, ϕ::OrdinalMap)
    print(io, typeof(ϕ), "(")
    show(IOContext(io, :compact=>true), ϕ.domain)
    print(io, ")")
end

function _print_mapped_pair(f, io::IO, a, b)
    print(io, ' '^4)
    f(io, a)
    printstyled(" → ", color=:magenta)
    f(io, b)
end

function Base.show(io::IO, ::MIME"text/plain", ϕ::OrdinalMap)
    printstyled(io, typeof(ϕ), bold=true, color=:cyan)
    print(io, ":\n")
    for s ∈ domain(ϕ)
        _print_mapped_pair(show, io, s, ϕ(s))
        println(io)
    end
end

function Base.show(io::IO, Φ::MultiOrdinalMap)
    print(io, typeof(Φ), "(")
    show(io, Φ.maps)
    print(io, ")")
end

function Base.show(io::IO, ::MIME"text/plain", Φ::MultiOrdinalMap)
    max_vals = 40  # obviously this is a shit way of doing this, but I'm lazy
    printstyled(io, typeof(Φ), bold=true, color=:cyan)
    print(io, ":\n")
    j = 0
    for s ∈ domain(Φ)
        j += 1
        if j > max_vals
            _print_mapped_pair(print, io, "⋯", "⋯")
            break
        end
        _print_mapped_pair(show, io, s, Φ(s))
        println(io)
    end
end
