# MiscDataTools

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/MiscDataTools.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/MiscDataTools.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/MiscDataTools.jl/-/pipelines)

Experimental toolbox of tools I very commonly need during the "data mangling" phase of data science
projects.  This package will probably never be registered, but one day I may spin it off into
something more mature.
