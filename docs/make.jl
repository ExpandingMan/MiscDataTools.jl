using MiscDataTools
using Documenter

DocMeta.setdocmeta!(MiscDataTools, :DocTestSetup, :(using MiscDataTools); recursive=true)

makedocs(;
    modules=[MiscDataTools],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/MiscDataTools.jl/blob/{commit}{path}#{line}",
    sitename="MiscDataTools.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/MiscDataTools.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
