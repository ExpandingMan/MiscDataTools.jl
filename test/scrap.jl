using MiscDataTools

using MiscDataTools: flatten, flattened, unflatten


qmain() = quote
    nt = (a=["a", "b", "c"], b=["a", "b"], c=["a"])
    Φ = MultiOrdinalMap(nt)
end
